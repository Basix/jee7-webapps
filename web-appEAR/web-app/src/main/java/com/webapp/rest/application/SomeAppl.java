package com.webapp.rest.application;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("v1")
public class SomeAppl extends Application {
}