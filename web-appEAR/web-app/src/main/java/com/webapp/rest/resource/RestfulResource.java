package com.webapp.rest.resource;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/testme")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class RestfulResource
{
    @GET
    public Response testEndPoint(@QueryParam("test") String var )
    {
        System.out.println("HELLO WOLRD REST SVC" + var);
        return Response.ok(var).build();
    }
}